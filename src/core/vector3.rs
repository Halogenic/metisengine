
use core::float::sqrt;
use core::ops::{Add, Div, Mul, Neg, Sub};
use core::to_str::ToStr;

// put these into a more central location so that
// they can be accessed from other structure
// definitions (like Quaternion/Matrix)

trait AddTo<L> {
    fn add_to(&self, lhs: &L) -> L;
}

trait SubFrom<L> {
    fn sub_from(&self, lhs: &L) -> L;
}

trait MulWith<L> {
    fn mul_with(&self, lhs: &L) -> L;
}

trait DivInto<L> {
    fn div_into(&self, lhs: &L) -> L;
}

pub struct Vector3 {
    pub x: float,
    pub y: float,
    pub z: float
}

impl Vector3 {
    fn new(x: float, y: float, z: float) -> Vector3 {
        Vector3 { x: x, y: y, z: z }
    }

    fn length(&self) -> float {
        sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
    }

    fn length_sqr(&self) -> float {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    fn normalise(&mut self) {
        let length = self.length();

        self.x /= length;
        self.y /= length;
        self.z /= length;
    }

    // static constructors

    fn zero() -> Vector3 {
        Vector3 { x: 0f, y: 0f, z: 0f }
    }

    fn up() -> Vector3 {
        Vector3 { x: 0f, y: 1f, z: 0f }
    }

    fn down() -> Vector3 {
        -Vector3::up()
    }

    fn left() -> Vector3 {
        Vector3 { x: 1f, y: 0f, z: 0f }
    }

    fn right() -> Vector3 {
        -Vector3::left()
    }

    fn forward() -> Vector3 {
        Vector3 { x: 0f, y: 0f, z: -1f }
    }

    fn back() -> Vector3 {
        -Vector3::forward()
    }

    // static methods

    fn dot(v1: &Vector3, v2: &Vector3) -> float {
        v1.x * v2.x + v1.y * v2.y + v1.z * v2.z
    }

    fn cross(v1: &Vector3, v2: &Vector3) -> Vector3 {
        Vector3 {
            x: v1.y * v2.z - v1.z * v2.y,
            y: v1.z * v2.x - v1.x * v2.z,
            z: v1.x * v2.y - v1.y * v2.x
        }
    }

    fn normalised(v: &Vector3) -> Vector3 {
        let length = v.length();

        Vector3 {
            x: v.x / length,
            y: v.y / length,
            z: v.z / length
        }
    }

}

impl ToStr for Vector3 {
    fn to_str(&self) -> ~str {
        fmt!("{x: %f} {y: %f} {z: %f} ", 
              self.x, self.y, self.z)
    }
}

impl AddTo<Vector3> for Vector3 {
    fn add_to(&self, lhs: &Vector3) -> Vector3 {
        Vector3 {
            x: lhs.x + self.x,
            y: lhs.y + self.y,
            z: lhs.z + self.z
        }
    }
}

impl AddTo<Vector3> for float {
    fn add_to(&self, lhs: &Vector3) -> Vector3 {
        Vector3 {
            x: self + lhs.x,
            y: self + lhs.y,
            z: self + lhs.z
        }
    }
}

impl<R: AddTo<Vector3>> Add<R, Vector3> for Vector3 {
    fn add(&self, rhs: &R) -> Vector3 {
        rhs.add_to(self)
    }
}

impl SubFrom<Vector3> for Vector3 {
    fn sub_from(&self, lhs: &Vector3) -> Vector3 {
        Vector3 {
            x: lhs.x - self.x,
            y: lhs.y - self.y,
            z: lhs.z - self.z
        }
    }
}

impl SubFrom<Vector3> for float {
    fn sub_from(&self, lhs: &Vector3) -> Vector3 {
        Vector3 {
            x: lhs.x - *self,
            y: lhs.y - *self,
            z: lhs.z - *self
        }
    }
}

impl<R: SubFrom<Vector3>> Sub<R, Vector3> for Vector3 {
    fn sub(&self, rhs: &R) -> Vector3{
        rhs.sub_from(self)
    }
}

impl MulWith<Vector3> for Vector3 {
    fn mul_with(&self, lhs: &Vector3) -> Vector3 {
        Vector3::cross(lhs, self)
    }
}

impl MulWith<Vector3> for float {
    fn mul_with(&self, lhs: &Vector3) -> Vector3 {
        Vector3 {
            x: lhs.x * *self,
            y: lhs.y * *self,
            z: lhs.z * *self
        }
    }
}

impl<R: MulWith<Vector3>> Mul<R, Vector3> for Vector3 {
    fn mul(&self, rhs: &R) -> Vector3 {
        rhs.mul_with(self)
    }
}

impl Div<float, Vector3> for Vector3 {
    fn div(&self, rhs: &float) -> Vector3 {
        Vector3 { 
            x: self.x / *rhs, 
            y: self.y / *rhs, 
            z: self.z / *rhs 
        }
    }
}

impl Neg<Vector3> for Vector3 {
    fn neg(&self) -> Vector3 {
        Vector3 { 
            x: -self.x, 
            y: -self.y, 
            z: -self.z 
        }
    }
}


fn main() {
    let mut v = Vector3::zero();
    v += Vector3::new(15f, 2f, 12f);

    v += 2f;

    println(v.to_str());
}
