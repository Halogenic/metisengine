
use core::to_str::ToStr;

pub struct Colour {
    r: float;
    g: float;
    b: float;
    a: float;
}

impl Colour {
    fn new(r: float, g: float, b: float, a: float) -> Colour {
        Colour { r: r, g: g, b: b, a: a }
    }
}

impl ToStr for Colour {
    fn to_str(&self) -> ~str {
        fmt!("{r: %f} {g: %f} {b: %f} {a: %f}",
              self.r, self.g, self.b, self.a)
    }
}
